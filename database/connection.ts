import { Sequelize } from 'sequelize';

const connection = new Sequelize(String(process.env.DEV_DB_NAME), String(process.env.DEV_DB_USERNAME), String(process.env.DEV_DB_PASSWORD), {
    host: process.env.DEV_DB_HOSTNAME,
    dialect: 'mysql',
    logging: false
});

export default connection;