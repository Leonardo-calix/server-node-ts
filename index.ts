import express, { Application } from 'express';
import dotenv from 'dotenv'
import cors from 'cors';

dotenv.config();
const app: Application = express();
const port = process.env.PORT || 4000;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

//import router
import appRouter from './routers/app-router';
import connection from './database/connection';
import usersRouter from './routers/users.router';
import productsRouter from './routers/products-router';



//Routes
app.use('/', appRouter);
app.use('/users', usersRouter);
app.use('/products', productsRouter);



app.listen(port, () => {
    try {
        connection.sync();
        console.log(`Server on port ${port}`);
        console.log('DB is connected')
    } catch (error) {
        console.error(error)
    }
}) 