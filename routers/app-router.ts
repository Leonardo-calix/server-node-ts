import { Router } from 'express';
import { app } from '../controllers/app';

const router = Router();

router.get('/', app );


export default router;