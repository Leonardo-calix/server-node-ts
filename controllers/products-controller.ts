import { Request, Response } from 'express';
import Products from '../models/Product';
import Users from '../models/User';


export const getProducts = async (req: Request, res: Response) => {
    const productsList = await Products.findAll({
        include:[
            {
                model: Users
            }
        ]
    });
    res.json(productsList);
}

// export const getUser = async (req: Request, res: Response) => {
//     const user = await Products.findByPk(req.params.id);
//     if (!user) return res.status(404).json({ message: 'Registro no existe' });
//     res.json(user);
// }

// export const createUser = async (req: Request, res: Response) => {

//     const { body } = req;

//     const user = await Products.create({
//         firstName: body.firstName,
//         lastName: body.lastName,
//         email: body.email
//     });

//     res.json(user);
// }

