import { Request, Response } from 'express';
import Product from '../models/Product';
import Users from '../models/User'

export const getUsers = async (req: Request, res: Response) => {
    const usersList = await Users.findAll({
        include:[
            {
                model: Product
            }
        ]
    });
    res.json(usersList);
}

export const getUser = async (req: Request, res: Response) => {
    const user = await Users.findByPk(req.params.id);
    if (!user) return res.status(404).json({ message: 'Registro no existe' });
    res.json(user);
}

export const createUser = async (req: Request, res: Response) => {

    const { body } = req;

    const user = await Users.create({
        firstName: body.firstName,
        lastName: body.lastName,
        email: body.email
    });

    res.json(user);
}

