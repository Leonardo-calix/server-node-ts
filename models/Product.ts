import { DataTypes } from 'sequelize';
import connection from '../database/connection';
import User from './User';

const Product = connection.define('Product', {
    name: {
        type: DataTypes.STRING
    },
    price: {
        type: DataTypes.DOUBLE
    },
    brands: {
        type: DataTypes.STRING
    },
    state: {
        type: DataTypes.BOOLEAN
    }
},
    {
        timestamps: false
    }
);

//
//Product.belongsToMany(User);


export default Product;

