import { DataType, DataTypes } from 'sequelize';
import connection from '../database/connection';
import Product from './Product';

const User = connection.define('User', {
    firstName: {
        type: DataTypes.STRING
    },
    lastName: {
        type: DataTypes.STRING
    },
    email: {
        type: DataTypes.STRING
    },
    state: {
        type: DataTypes.BOOLEAN
    }
},
    {
        timestamps: false
    }
);

// asociociones
User.hasMany(Product);



export default User;

